package u05lab.code

import org.scalatest.funsuite.AnyFunSuite

class ExamManagerTest extends AnyFunSuite {

  private def setupExm(em: ExamsManager):Unit = {
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")

    em.addStudentResult("gennaio", "rossi", ExamResultFactory.failed) // rossi -> fallito
    em.addStudentResult("gennaio", "bianchi", ExamResultFactory.retired) // bianchi -> ritirato
    em.addStudentResult("gennaio", "verdi", ExamResultFactory.succeeded(28)) // verdi -> 28
    em.addStudentResult("gennaio", "neri", ExamResultFactory.cumLaude) // neri -> 30L

    em.addStudentResult("febbraio", "rossi", ExamResultFactory.failed) // etc..
    em.addStudentResult("febbraio", "bianchi", ExamResultFactory.succeeded(20))
    em.addStudentResult("febbraio", "verdi", ExamResultFactory.succeeded(30))

    em.addStudentResult("marzo", "rossi", ExamResultFactory.succeeded(25))
    em.addStudentResult("marzo", "bianchi", ExamResultFactory.succeeded(25))
    em.addStudentResult("marzo", "viola", ExamResultFactory.failed)
  }


  test("Basic test") {
    val em: ExamsManager = ExamsManager()
    setupExm(em)
    // partecipanti agli appelli di gennaio e marzo
    assert(em.getAllStudentsFromCall("gennaio") == Set("rossi", "bianchi", "verdi", "neri"))
    assert(em.getAllStudentsFromCall("marzo") == Set("rossi", "bianchi", "viola"))
    // promossi di gennaio con voto
    assert(em.getEvaluationsMapFromCall("gennaio").size == 2)
    assert(em.getEvaluationsMapFromCall("gennaio")("verdi") == "28")
    assert(em.getEvaluationsMapFromCall("gennaio")("neri") == "30")

    // promossi di febbraio con voto
    assert(em.getEvaluationsMapFromCall("febbraio").size == 2)
    assert(em.getEvaluationsMapFromCall("febbraio")("bianchi") == "20")
    assert(em.getEvaluationsMapFromCall("febbraio")("verdi") == "30")
    // tutti i risultati di rossi (attenzione ai toString!!)
    assert(em.getResultsMapFromStudent("rossi").size == 3)
    assert(em.getResultsMapFromStudent("rossi")("gennaio") == "FAILED")
    assert(em.getResultsMapFromStudent("rossi")("febbraio") == "FAILED")
    assert(em.getResultsMapFromStudent("rossi")("marzo") == "SUCCEEDED(25)")
    // tutti i risultati di bianchi
    assert(em.getResultsMapFromStudent("bianchi").size == 3)
    assert(em.getResultsMapFromStudent("bianchi")("gennaio") == "RETIRED")
    assert(em.getResultsMapFromStudent("bianchi")("febbraio") == "SUCCEEDED(20)")
    assert(em.getResultsMapFromStudent("bianchi")("marzo") == "SUCCEEDED(25)")
    // tutti i risultati di neri
    assert(em.getResultsMapFromStudent("neri").size == 1)
    assert(em.getResultsMapFromStudent("neri")("gennaio") == "SUCCEEDED(30L)")
  }

  test("optional exercise") {
    val em: ExamsManager = ExamsManager()
    setupExm(em)
    assert(em.getBestResultFromStudent("rossi").contains(25))
    assert(em.getBestResultFromStudent("bianchi").contains(25))
    assert(em.getBestResultFromStudent("neri").contains(30))
    assert(em.getBestResultFromStudent("viola").isEmpty)
  }

  test("can't create call twice") {
    val em: ExamsManager = ExamsManager()
    assertThrows[IllegalArgumentException] {
      em.createNewCall("a")
      em.createNewCall("a")
    }
  }

  test("can't register before open call") {
    val em: ExamsManager = ExamsManager()
    assertThrows[IllegalStateException] {
      em.addStudentResult("a", "b", ExamResultFactory.failed)
    }
  }

  test("can't evaluate twice") {
    val em: ExamsManager = ExamsManager()
    assertThrows[IllegalArgumentException] {
      em.createNewCall("a")
      em.addStudentResult("a", "b", ExamResultFactory.failed)
      em.addStudentResult("a", "b", ExamResultFactory.succeeded(29))
    }
  }





}
