package u05lab.code

import org.scalatest.funsuite.AnyFunSuite
import u04lab.code.{ExamResultFactory, Kind}


class ExamResultFactoryTest extends AnyFunSuite {

  test("basic failed") {
    val failed = ExamResultFactory.failed
    assert(failed.kind == Kind.FAILED)
    assert(failed.evaluation.isEmpty)
    assert(!failed.cumLaude)
  }

  test("basic retired"){
    val retired = ExamResultFactory.retired
    assert(retired.kind == Kind.RETIRED)
    assert(retired.evaluation.isEmpty)
    assert(!retired.cumLaude)
  }


  test("basic succeeded") {
    val succeeded = ExamResultFactory.succeeded(18)
    assert(succeeded.kind == Kind.SUCCEEDED)
    assert(succeeded.evaluation.isDefined)
    assert(succeeded.evaluation.get == 18)
    assert(!succeeded.cumLaude)
  }

  test("succeeded exceptions") {
    assertThrows[IllegalArgumentException] {
      ExamResultFactory.succeeded(17)
      ExamResultFactory.succeeded(31)
    }
  }

  test("basic cum laude"){
    val cumLaude = ExamResultFactory.cumLaude
    assert(cumLaude.kind == Kind.SUCCEEDED)
    assert(cumLaude.evaluation.isDefined)
    assert(cumLaude.evaluation.get == 30)
    assert(cumLaude.cumLaude)
  }



}
