package u05lab.code

/*
*QUESTO TRAIT DEFINISCE I METODI CHE SONO DISPONIBILI PER UN RESULT
* DENTRO AVRÒ ANCHE IL RIFERIMETNO AL KIND DEL VOTO
*
* LA FACTORY RITORNA ISTANZE DI QUESTO TRAIR
* */
object Kind extends Enumeration {val FAILED, RETIRED, SUCCEEDED = Value}

case class ExamResult(val kind: Kind.Value, val evaluation: Option[Int], val cumLaude: Boolean) {
  override def toString: String = kind.toString concat (if (kind == Kind.SUCCEEDED) {if (cumLaude) "(30L)" else s"(${evaluation.get})"} else "")
}

trait ExamResultFactory {
  def failed: ExamResult
  def retired: ExamResult
  def succeeded(mark: Int): ExamResult
  def succeededCumLaude: ExamResult
}

object ExamResultFactory{
  def failed:ExamResult = ExamResult(Kind.FAILED, None, cumLaude = false)
  def retired:ExamResult = ExamResult(Kind.RETIRED, None, cumLaude = false)
  def cumLaude:ExamResult = ExamResult(Kind.SUCCEEDED, Some(30), cumLaude = true)
  def succeeded(mark: Int):ExamResult = if (mark >= 18 && mark <= 30) ExamResult(Kind.SUCCEEDED, Some(mark), cumLaude = false) else throw new IllegalArgumentException
}

trait ExamsManager {

  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, result: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationsMapFromCall(call: String): Map[String, String]

  def getResultsMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]

  def apply(): ExamsManager = new ExamsManagerImpl()
}


/*Qui dentro implementare i metodi*/
private case class ExamsManagerImpl() extends ExamsManager {

  private var data: Map[String, Map[String, ExamResult]] = Map()

  override def createNewCall(call: String): Unit = {
    if (data contains call) throw new IllegalArgumentException
    data = data + (call -> Map[String, ExamResult]())
  }

  override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
    if (!data.contains(call)) throw new IllegalStateException
    if(data(call).contains(student)) throw new IllegalArgumentException
    data = data.updated(call, data(call) + (student -> result))
  }

  override def getAllStudentsFromCall(call: String): Set[String] = data(call) keySet

  /*
  * must return only if student has succeded the exam
  * */
  override def getEvaluationsMapFromCall(call: String): Map[String, String] =
    //data(call).filter(_._2.kind == Kind.SUCCEEDED).mapValues(_.evaluation.get.toString)
    data(call).collect {
      case (s: String, r: ExamResult) if r.kind == Kind.SUCCEEDED => (s, r.evaluation.get.toString)
    }

  /*
  * mappa call -> risultato
  * */
  override def getResultsMapFromStudent(student: String): Map[String, String] =
    /*
    * prima di tutto tengo solamente le call in cui compare lo studente in oggetto
    * successivamente i valori vengono mappati
    *
    * i valori sono mappe[String, ExamResult]
    * devono divetare stringhe che rappresentano il risultato del valore in esame
    *
    * ogni mappa ha solamente uno studente che mi interessa e sono certo che esista
    *   quindi posso gettarlo per ottenere subito il suo exam result
    *     poi gli applico il to string per ottenere la rappresentazione che voglio
    *
    * */
    //data.filter(_._2.keySet contains student).mapValues(_(student).toString)
    //data.filter(_._2 contains student).mapValues(_(student).toString)

    data collect {
      case (call, map) if map contains student => (call, map(student).toString)
    }

  override def getBestResultFromStudent(student: String): Option[Int] =
    /*
    data.filter(_._2.keySet contains student).mapValues(_(student).evaluation).values.filter(_.isDefined) match {
      case markList if markList.nonEmpty=> Some(markList.map(_.get).max)
      case _ => None
    }
    */
    data collect {
      case (_, marks) if (marks contains student) && (marks(student).evaluation.isDefined) => marks(student).evaluation.get
    } match {
      case markList if markList.nonEmpty=> Some(markList.max)
      case _ => None
    }



}

object ExamsManager {
  def apply(): ExamsManager = ExamsManagerImpl()
}


/*
  Man mano che si lavora aggiugnere i test
* */

object ExamsManagerTest extends App {
}